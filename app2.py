from flask import Flask, jsonify, request, session, json, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.expression import func
import redis

redi = redis.Redis(host='localhost')
redi.rpush("connlist", "127.0.0.1:3002")

app = Flask(__name__)
DATABASE = 'cmpe_273'
PASSWORD = 'cmpe_273'
USER = 'cmpe_273'
HOSTNAME = 'localhost'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@%s/%s'%(USER, PASSWORD, HOSTNAME, DATABASE)
db = SQLAlchemy(app)


def CreateDB():
    def __init__(self, hostname=None):
    	if hostname != None:	
    		HOSTNAME = hostname
    import sqlalchemy
    engine = sqlalchemy.create_engine('mysql://%s:%s@%s'%(USER, PASSWORD, HOSTNAME)) 
    engine.execute("CREATE DATABASE IF NOT EXISTS %s "%(DATABASE)) 


class Create_Expenses(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False)
    email = db.Column(db.String(120), unique=False)
    category = db.Column(db.String(120), unique=False)
    description = db.Column(db.String(120), unique=False)
    link = db.Column(db.String(200), unique=False)
    estimated_costs = db.Column(db.String(200), unique=False)
    submit_date = db.Column(db.String(80), unique=False)
    status = db.Column(db.String(120), unique=False)
    decision_date = db.Column(db.String(80), unique=False)
 
    def __init__(self, name, email, category, description, link, estimated_costs,
    submit_date, status, decision_date):
        self.name = name
        self.email = email
        self.category = category
        self.description = description
        self.link = link
        self.estimated_costs = estimated_costs
        self.submit_date = submit_date
        self.status = status
        self.decision_date = decision_date


@app.route('/v1/expenses/<string:id>', methods=['GET'])
def getExpense(id):
    one = Create_Expenses.query.filter_by(id=id).first_or_404()
    one_get_id = {
    'id' : one.id,
    'name' : one.name,
    'email' : one.email,
    'category' : one.category,
    'description' : one.description,
    'link' : one.link,
    'estimated_costs' : one.estimated_costs,
    'submit_date' : one.submit_date,
    'status' : "pending",
    'decision_date' : "09-10-2016"    
    }
    
    response = jsonify(one_get_id)
    response.status_code = 200
    return response

@app.route('/v1/expenses', methods=['GET'])
def get_expense():
    res_full = []
    res_full = Create_Expenses.query.all()
    results =[]
    for res in res_full:
        result_get = {
        'id' : res.id,
        'name' : res.name,
        'email' : res.email,
        'category' : res.category,
        'description' : res.description,
        'link' : res.link,
        'estimated_costs' : res.estimated_costs,
        'submit_date' : res.submit_date,
        'status' : "pending",
        'decision_date' : "09-10-2016"    
        }
        results.append(result_get)
    
    response = jsonify(results)
    response.status_code = 200
    return response

    
@app.route('/v1/expenses', methods=['POST'])
def postt_expense():
    CreateDB()
    db.create_all()
    request_json = request.get_json(force=True)
    expense = Create_Expenses(request.json['name'],request.json['email'],
    request.json['category'],request.json['description'],request.json['link']
    ,request.json['estimated_costs'],request.json['submit_date'],
    'pending|approved|rejected|overbudget','09-10-2016')

    db.session.add(expense)
    db.session.commit()

    id_res = db.session.query(func.max(Create_Expenses.id))
    res = Create_Expenses.query.filter_by(id=id_res).first()
    result_query = {
    'id' : res.id,
    'name' : res.name,
    'email' : res.email,
    'category' : res.category,
    'description' : res.description,
    'link' : res.link,
    'estimated_costs' : res.estimated_costs,
    'submit_date' : res.submit_date,
    'status' : "pending",
    'decision_date' : "09-10-2016"    
    }

    response = jsonify(result_query)
    response.status_code = 201
    return response

@app.route('/v1/expenses/<string:id>', methods=['PUT'])
def updateExpense(id):  
    request_json=request.get_json(force=True)
    response = Create_Expenses.query.filter_by(id=id).first_or_404()
    response.estimated_costs = request_json['estimated_costs']  
    db.session.commit()    
    update_query= {
    'estimated_costs' : response.estimated_costs   
    }
    response = jsonify(update_query)
    response.status_code = 202
    return response

@app.route('/v1/expenses/<string:id>', methods=['DELETE'])
def deleteExpense(id):
    
    dele = Create_Expenses.query.filter_by(id=id).first()
    db.session.delete(dele)
    db.session.commit()
    response = Response(status=204, mimetype='application/json')
    return response

if __name__ == '__main__':
    app.run(debug=False,host='localhost', port='3002')

import socket
import select
import sys
from time import sleep
import redis
from cb import CircuitBreaker

redi = redis.Redis(host='localhost')

buffer_size = 4096
delay = 0.0001
forward_to = ('localhost', 5000)
_failure_count = 0
threshold = 3
class Forward:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self, host, port):
        while True:
		try:
		    
		    self.forward.connect((host, int(port)))
		    return self.forward
		except Exception, e:
		  
		    print "Unable to reach server, Call Circuit Breaker!"
                    run()
		    server.main_loop()


class TheServer(object):
    input_list = [1]
    channel = {}
    def __init__(self, host, port):
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.server.bind((host, port))
            self.server.listen(200)

    def main_loop(self):
        self.input_list.append(self.server)
        while 1:	   
            ss = select.select
            inputready, outputready, exceptready = ss(self.input_list, [], [])
            for self.s in inputready:
		print self.s
                if self.s == self.server:
                    self.on_accept()
                    break

                self.data = self.s.recv(buffer_size)
                if len(self.data) == 0:
                    self.on_close()
                    break
                else:
                    self.on_recv()

    def on_accept(self): 
	_failure_count = 0
	if _failure_count < threshold:	
		host , port = get_server()
		forward = Forward().start(host, port)
		clientsock, clientaddr = self.server.accept()		
		if forward:
			
			self.input_list.append(clientsock)
			self.input_list.append(forward)
			self.channel[clientsock] = forward
			self.channel[forward] = clientsock
		else:
			print "Can't establish connection with remote server.",
			print "Closing connection with client side", clientaddr
			clientsock.close()
			_failure_count += 1 
	else:   	
            	raise Exception(err)
                

    def on_close(self):
        self.input_list.remove(self.s)
        self.input_list.remove(self.channel[self.s])
        out = self.channel[self.s]
        self.channel[out].close()  
        self.channel[self.s].close()
        del self.channel[out]
        del self.channel[self.s]

    def on_recv(self):
        data = self.data
        self.channel[self.s].send(data)

def get_server():

	redi.rpoplpush("connlist","connlist")
	try:	
		server1=redi.lindex("connlist",0)
		if server1:
			print "connecting to server: %s " %server1
			host, port=server1.split(":")
		else:
			print "No more server in db!!"
			sys.exit(1)
	except Exception, e:
		print "e"
	return host, port


@CircuitBreaker(max_failure_to_open=3, reset_timeout=10)
def dependency_call(call_num):
def run():
    num_success = 0
    num_failure = 0
    num_circuitbreaker_passthrough_failure = 0
    for i in range(1,4):
        try:
            print "Call-%d:" % i
            print "Result=%s" %dependency_call(i)
            num_success += 1
        except Exception as ex:
            num_failure += 1
            if ex.message == MY_EXCEPTION:
                num_circuitbreaker_passthrough_failure += 1
            print ex.message
        sleep(0.5)
    return num_success, num_failure, num_circuitbreaker_passthrough_failure

if __name__ == '__main__':
        server = TheServer('127.0.0.1', 8080)
        try:
            server.main_loop()
        except KeyboardInterrupt:
            print "Ctrl C - Stopping server"
sys.exit(1)